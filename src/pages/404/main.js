import { createApp } from 'petite-vue';

import './styles/main.scss';

createApp({}).mount('#app');
